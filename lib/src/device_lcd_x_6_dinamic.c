/* INSERT HEADER */

#include <avr/io.h>
#include <string.h>
#include <avr/cpufunc.h>
#include <avr/pgmspace.h>
#include <util/delay_basic.h>

#include "device_lcd_x_6_dinamic.h"

static lcd_input_buffer_value_t lcd_input_buffer[LCD_DIGITS_COUNT];
/*accelerate access to this buffer*/
static lcd_output_buffer_value_t lcd_output_buffer[LCD_DIGITS_COUNT];
/*
    \about fill output buffer with default value
    \in no input
*/
static void lcd_clear_output_buffer( void )
{
    memset(lcd_output_buffer, LCD_BLANK_DIGIT, LCD_DIGITS_COUNT);
}

static void lcd_clear_input_buffer( void )
{
    memset(lcd_input_buffer, LCD_SYMBOL_SPACE, LCD_DIGITS_COUNT);
}

/*
for display used:
2 pcs of HDSP-B02E with 3 digits each [A1, A2, A3] with 7+1 segments [a,b,c,d,e,f,g, DP]
1 pcs ULN2804 after SN74HC573 to latch and pull to 0 segments (see table for decode)
> 1 - segment is ON; 0 - digit selected
1 pcs SN74HC573 to latch and open transistors to sours with input voltage selected digits

Table represent conection map
    |------------|-----------|-------|-----|-----|
    | bit/pin #  | Port name | U3/U5 | LE1 | LE2 |
    |------------|-----------|-------|-----|-----|
    |   0 / 23   |   PC0     |   D1  | A   | xx  |
    |   1 / 24   |   PC1     |   D2  | B   | BZ  |
    |   2 / 25   |   PC2     |   D3  | C   | D3  |
    |   3 / 26   |   PC3     |   D4  | D   | D4  |
    |------------|-----------|-------|-----|-----|
    |   4 / 6    |   PD4     |   D5  | E   | D5  |
    |   5 / 11   |   PD5     |   D6  | F   | D0  |
    |   6 / 12   |   PD6     |   D7  | G   | D1  |
    |   7 / 13   |   PD7     |   D8  | DP  | D2  |
    |------------|-----------|-------|-----|-----|
    |     / 14   |   PB0     |   LE  | /\  |     |
    |     / 15   |   PB1     |   LE  |     | /\  |
    |     / 16   |   PB2     |  \OE  |     |     |  pull down externally, [1 - OFF]
    |------------|-----------|-------|-----|-----|
    * BZ - buzzer [1 - on]
    * Dn - selected digit [1 - on]
    * [A..G,DP] - segments


*/
/*TODO: reuse macros force_register_bit_to, move it globally*/
#define lcd_latch_segments(x) \
    { \
    if (x != 0) \
            PORTB |= _BV(PB0); \
    else \
            PORTB &= ~(_BV(PB0)); \
    }

#define lcd_latch_digits(x) \
    { \
    if (x != 0) \
            PORTB |= _BV(PB1); \
    else \
            PORTB &= ~(_BV(PB1)); \
    }

#define lcd_latch_enable(x) \
    { \
    if (x == 0) \
            PORTB |= _BV(PB2); \
    else \
            PORTB &= ~(_BV(PB2)); \
    }


static void lcd_push_to_data_bus(uint8_t byte)
{
    PORTD &= ~(0xF0);
    PORTD |=  (0xF0 & byte);
    
    PORTC &= ~(0x0F);
    PORTC |=  (0x0F & byte);
}

static void lcd_init_io_ports()
{
    /* always zero, togle only to latch data*/
    lcd_latch_segments(0);
    lcd_latch_digits(0);
    lcd_latch_enable(0);
    DDRB  |= _BV(DDB0)  | _BV(DDB1)  | _BV(DDB2);

    lcd_push_to_data_bus(LCD_BLANK_DIGIT);
    /*data port could be uninited before pushing data there */
    DDRD  |= _BV(DDD4)  | _BV(DDD5)  | _BV(DDD6)  | _BV(DDD7);
    DDRC  |= _BV(DDC0)  | _BV(DDC1)  | _BV(DDC2)  | _BV(DDC3);
}

#if 0
static void lcd_deinit_io_ports()
{
    /*TODO: implement if needed*/
}
#endif

#define lcd_local_fixed_pause() \
    { \
    /*_NOP();*/ \
    }

static void lcd_show_byte_from_buffer(uint8_t pos)
{
    uint8_t segments =  lcd_output_buffer[pos];
    uint8_t digit = pgm_read_byte(&(lcd_digit_decode_table_arr[pos]));
    /*TODO: insert buzzer bit manipulation here
        like:
        if (0 != buzzer_on) digits |= _BV(6);
        if (0 != extrap_on) digits |= _BV(7);
    */

    lcd_push_to_data_bus(digit);
    lcd_latch_digits(1);
        lcd_local_fixed_pause();  /*test if really necessary*/
    lcd_latch_digits(0);

    lcd_push_to_data_bus(segments);
    lcd_latch_segments(1);
        lcd_local_fixed_pause();  /*test if really necessary*/
    lcd_latch_segments(0);
    #if (LCD_BLANKING_METHOD == 0)
    lcd_latch_enable(1);    /* re-enable outputs */
    #endif
}

static void lcd_blank()
{
    #if (LCD_BLANKING_METHOD == 0)
    lcd_latch_enable(0);
    #else
    lcd_push_to_data_bus(LCD_BLANK_DIGIT);
    lcd_latch_segments(1);
        lcd_local_fixed_pause();  /*test if really necessary*/
    lcd_latch_segments(0);
    #endif
}

static void lcd_update_output_buffer_from_input(lcd_input_buffer_value_t (*in)[], lcd_output_buffer_value_t (*out)[])
{
    int n = 0;
    do
    {
        (*out)[n] = pgm_read_byte(&(lcd_segments_decode_table_arr[ (*in)[n].lowbits ]));
    } while (++n < LCD_DIGITS_COUNT);
}

//static active_digit

void lcd_upgrade_string(void)
{
    lcd_update_output_buffer_from_input( &lcd_input_buffer, &lcd_output_buffer );
}


void lcd_init()
{
    lcd_init_io_ports();
    lcd_clear_output_buffer();

    /*OPTIONALLY: refrash full string once.
        or as lees memory alternative:

        lcd_show_byte_from_buffer(0);
    */
    lcd_upgrade_string();
    /*configure autorefrash*/

    /*last operation - enabled external register's outputs*/
    lcd_latch_enable(1);
}


void lcd_upgrade_next_digit(void)
{

}

/*
    !use lcd_upgrade_string() after;
    display value at 6x digits
    1 - sign -/+
    2-6 - 5 digits value with fixed point
*/
void lcd_show_value(int16_t val)
    {
    register uint16_t rp, dp, i = 0;
    _Bool zerooff_flag = 0;
    /*0*/
    lcd_clear_input_buffer();
    if(val < 0)
    {
        lcd_input_buffer[i].lowbits = LCD_SYMBOL_MINUS;
        rp = (uint16_t)(-val);
    }
    else
    {
        rp = (uint16_t)val;
    }
    i++;/*1*/
    dp = rp / 10000;
    rp = rp % 10000;
    if (dp!=0 || zerooff_flag) lcd_input_buffer[i].lowbits = (zerooff_flag=1, dp);
    i++;/*2*/
    dp = rp / 1000;
    rp = rp % 1000;
    if (dp!=0 || zerooff_flag) lcd_input_buffer[i].lowbits = (zerooff_flag=1, dp);
    i++;/*3*/
    dp = rp / 100;
    rp = rp % 100;
    /* fixed point here, no need hide last two zeroez*/
    lcd_input_buffer[i].dot_on = 1;
    if (dp!=0 || zerooff_flag) lcd_input_buffer[i].lowbits = (zerooff_flag=1, dp);
    i++;/*4*/
    dp = rp / 10;
    rp = rp % 10;
    lcd_input_buffer[i].lowbits = dp;
    i++;/*5*/
    lcd_input_buffer[i].lowbits = rp;
    }


void lcd_show_hex_register_at_addres(uint16_t addr, uint8_t val)
    {
        register uint16_t i = 0;
        lcd_input_buffer[i++].lowbits = 0x000F & (addr >> 8);
        lcd_input_buffer[i++].lowbits = 0x000F & (addr >> 4);
        lcd_input_buffer[i++].lowbits = 0x000F & (addr >> 0);
        lcd_input_buffer[i++].lowbits = LCD_SYMBOL_MINUS;
        lcd_input_buffer[i++].lowbits = 0x000F & (val >> 4);
        lcd_input_buffer[i++].lowbits = 0x000F & (val >> 0);
    }
/*
===========================================================================
## TEST: once display data from char buffer
===========================================================================
*/
void lcd_test_string_output(void (*pause)(), uint8_t (*buf_to_display)[] )
{
    static const uint8_t LCD_TEST_PWM = 1;
    uint8_t num = 0;

    lcd_update_output_buffer_from_input(
        (lcd_input_buffer_value_t (*)[LCD_DIGITS_COUNT] ) buf_to_display,
        &lcd_output_buffer );

    do {
        lcd_show_byte_from_buffer(num);
        (*pause)();
        lcd_blank();
        for(int i=0; i < LCD_TEST_PWM; i++) (*pause)();
    } while (++num < LCD_DIGITS_COUNT);
}

/*
    fast decode of input buffer
    - using decode table
    - using mask to decode only low part of byte
    - depending from defines use short or extended decode table
    - using sign bit to show/or hide value


    DECODE BYTE:
    b7 leave blank
    b6
    b5
    b4  extended table / or error symbol specified additionaly
    g3  hex value limit
    b2
    b1
    b0
*/
//int lcd_fast_decode

void lcd_test_value_output(void)
{
    uint8_t num = 0;
    static uint16_t val = (-18462);

    lcd_show_value(val++);
    lcd_upgrade_string();
    do {
        lcd_show_byte_from_buffer(num);
        _delay_loop_1(200u);
        lcd_blank();
    } while (++num < LCD_DIGITS_COUNT);
}


void lcd_test_hex_output(void)
{
    uint8_t num = 0;
    static uint16_t val = (-18462);

    lcd_show_hex_register_at_addres(val >>4, val >>6);
    val++;
    lcd_upgrade_string();
    do {
        lcd_show_byte_from_buffer(num);
        _delay_loop_1(200u);
        lcd_blank();
    } while (++num < LCD_DIGITS_COUNT);
}
