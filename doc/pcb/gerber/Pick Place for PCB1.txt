Designator Footprint               Mid X         Mid Y         Ref X         Ref Y         Pad X         Pad Y TB      Rotation Comment        

R24        3216[1206]             860mil       1540mil        860mil       1540mil    915.118mil       1540mil  T        180.00 Res Semi       
R25        3216[1206]             420mil       1460mil        420mil       1460mil    475.118mil       1460mil  T        180.00 Res Semi       
R30        3216[1206]             640mil       1540mil        640mil       1540mil    695.118mil       1540mil  T        180.00 Res Semi       
C1         C1206                 2580mil       1880mil       2580mil       1880mil   2524.882mil       1880mil  T        360.00 Cap Semi       
C2         C1206                 2760mil       2020mil       2760mil       2020mil   2815.118mil       2020mil  T        180.00 Cap Semi       
Y2         PCBComponent_1_-_duplicate2     2587.5mil      882.5mil       2560mil        960mil       2560mil        960mil  T        270.00 XTAL           
Y1         PCBComponent_1_-_duplicate2     2777.5mil     1927.5mil       2700mil       1900mil       2700mil       1900mil  T        360.00 XTAL           
U?         NSO8_L                 900mil        600mil        900mil        600mil     992.52mil        525mil  T        180.00 MAX485CSA      
LS?        ABSM-1574          947.717mil   1144.724mil       1180mil       1140mil       1180mil       1140mil  T        180.00 Buzzer         
C7         C                      540mil        660mil        540mil        660mil        540mil    758.426mil  T        270.00 T494C          
C6         D                      280mil        100mil        280mil        100mil    405.984mil        100mil  T        180.00 Cap Pol1       
S4         PCBComponent_1_-_duplicate1       2230mil        260mil       1980mil        360mil       1980mil        360mil  T        270.00 SW-DPST        
S3         PCBComponent_1_-_duplicate1       1030mil        260mil        780mil        360mil        780mil        360mil  T        270.00 SW-DPST        
S2         PCBComponent_1_-_duplicate1       2830mil        260mil       2580mil        360mil       2580mil        360mil  T        270.00 SW-DPST        
S1         PCBComponent_1_-_duplicate1       1630mil        260mil       1380mil        360mil       1380mil        360mil  T        270.00 SW-DPST        
C3         C1206                 2440mil       1200mil       2440mil       1200mil       2440mil   1144.882mil  T         90.00 Cap Semi       
C8         C1206                  860mil        760mil        860mil        760mil    915.118mil        760mil  T        180.00 Cap Semi       
C9         C0805                  110mil        800mil        110mil        800mil     72.598mil        800mil  T        360.00 Cap Semi       
C10        C0805                   80mil        540mil         80mil        540mil     42.598mil        540mil  T        360.00 Cap Semi       
C11        C0805                  240mil        540mil        240mil        540mil    202.598mil        540mil  T        360.00 Cap Semi       
D9         SOD80C                1560mil        700mil       1560mil        700mil       1560mil    769.882mil  T        270.00 Diode 1N4148   
D10        SOD80C             1460.14mil    696.862mil   1460.139mil    696.861mil    1460.14mil     626.98mil  T         90.00 Diode 1N4148   
D11        SOD80C                1380mil        700mil       1380mil        700mil       1380mil    630.118mil  T         90.00 BZX79-C7V5     
D13        SOD80C                 700mil        720mil        700mil        720mil        700mil    650.118mil  T         90.00 BZX79-C7V5     
D15        SOD80C                1140mil        780mil       1140mil        780mil   1070.118mil        780mil  T        360.00 BZX79-C7V5     
DS1        PCBComponent_1_-_duplicate        770mil       1760mil       1020mil       2060mil       1020mil       2060mil  T        180.00 HDSP-B02E      
DS2        PCBComponent_1_-_duplicate       2350mil       1760mil       2600mil       2060mil       2600mil       2060mil  T        180.00 HDSP-B02E      
R13        2012[0805]            2180mil        720mil       2180mil        720mil   2142.598mil        720mil  T        360.00 Res Semi       
R16        2012[0805]            2340mil        940mil       2340mil        940mil       2340mil    902.598mil  T         90.00 Res Semi       
R17        2012[0805]            2320mil        720mil       2320mil        720mil   2357.402mil        720mil  T        180.00 Res Semi       
R20        2012[0805]            2860mil        900mil       2860mil        900mil       2860mil    937.402mil  T        270.00 Res Semi       
R22        2012[0805]            1140mil        620mil       1140mil        620mil   1177.402mil        620mil  T        180.00 Res Semi       
R23        2012[0805]            1420mil        560mil       1420mil        560mil   1382.598mil        560mil  T        360.00 Res Semi       
R33        RAD-0.1                460mil        280mil        460mil        280mil        510mil        280mil  T        180.00 Res Varistor   
R34        2012[0805]             740mil        860mil        740mil        860mil    777.402mil        860mil  T        180.00 Res Semi       
R35        2012[0805]             880mil        860mil        880mil        860mil    917.402mil        860mil  T        180.00 Res Semi       
R37        2012[0805]            1000mil        820mil       1000mil        820mil       1000mil    857.402mil  T        270.00 Res Semi       
R38        2012[0805]             120mil        660mil        120mil        660mil        120mil    622.598mil  T         90.00 Res Semi       
U2         SOP8_N                2240mil        580mil       2240mil        580mil   2338.426mil        505mil  T        180.00 M24C64MN6      
U4         28P3                  2380mil       1180mil       2380mil       1180mil       1730mil       1030mil  T        360.00 ATmega8L-8PC   
U7         SO8_L                  340mil        700mil        340mil        700mil     247.48mil        775mil  T        360.00 LM317LD        
D14        SOD80C                 580mil        120mil        580mil        120mil    649.882mil        120mil  T        180.00 D Zener        
BT1        PCBComponent_1_-_duplicate3    2640.52mil    1440.52mil       2340mil       1140mil       2340mil       1140mil  B         45.00 Battery        
C4         C0805                 2540mil        860mil       2540mil        860mil       2540mil    822.598mil  B         90.00 Cap Semi       
C5         C0805                 2620mil        860mil       2620mil        860mil       2620mil    822.598mil  B         90.00 Cap Semi       
D3         SOD80C                1610mil       1645mil       1610mil       1645mil   1540.118mil       1645mil  B        360.00 BZX79-C7V5     
D4         SOD80C                1610mil       1725mil       1610mil       1725mil   1540.118mil       1725mil  B        360.00 BZX79-C7V5     
D5         SOD80C                1610mil       1805mil       1610mil       1805mil   1540.118mil       1805mil  B        360.00 BZX79-C7V5     
D6         SOD80C                1320mil       1720mil       1320mil       1720mil       1320mil   1650.118mil  B         90.00 BZX79-C7V5     
D7         SOD80C                1240mil       1720mil       1240mil       1720mil       1240mil   1650.118mil  B         90.00 BZX79-C7V5     
D8         SOD80C                1160mil       1700mil       1160mil       1700mil       1160mil   1630.118mil  B         90.00 BZX79-C7V5     
Q3         318-08                1925mil       1585mil       1925mil       1585mil   1887.598mil   1634.212mil  B        360.00 BC857ALT1      
Q4         318-08                2080mil       1620mil       2080mil       1620mil   2042.598mil   1669.212mil  B        360.00 BC857ALT1      
Q5         318-08                2320mil       1780mil       2320mil       1780mil   2270.788mil   1742.598mil  B         90.00 BC857ALT1      
Q6         318-08                 620mil       1560mil        620mil       1560mil    582.598mil   1609.212mil  B        360.00 BC857ALT1      
Q7         318-08                 760mil       1560mil        760mil       1560mil    809.212mil   1597.402mil  B        270.00 BC857ALT1      
Q8         318-08                1000mil       1560mil       1000mil       1560mil    962.598mil   1609.212mil  B        360.00 BC857ALT1      
R6         2012[0805]            1920mil       1700mil       1920mil       1700mil   1882.598mil       1700mil  B        360.00 Res Semi       
R7         2012[0805]            1780mil       1640mil       1780mil       1640mil   1817.402mil       1640mil  B        180.00 Res Semi       
R8         2012[0805]            2080mil       1720mil       2080mil       1720mil   2042.598mil       1720mil  B        360.00 Res Semi       
R9         2012[0805]            1780mil       1720mil       1780mil       1720mil   1817.402mil       1720mil  B        180.00 Res Semi       
R10        2012[0805]            2200mil       1780mil       2200mil       1780mil       2200mil   1742.598mil  B         90.00 Res Semi       
R11        2012[0805]            1780mil       1800mil       1780mil       1800mil   1817.402mil       1800mil  B        180.00 Res Semi       
R12        2012[0805]             620mil       1660mil        620mil       1660mil    582.598mil       1660mil  B        360.00 Res Semi       
R14        2012[0805]            1220mil       1920mil       1220mil       1920mil   1182.598mil       1920mil  B        360.00 Res Semi       
R15        2012[0805]             880mil       1560mil        880mil       1560mil        880mil   1597.402mil  B        270.00 Res Semi       
R18        2012[0805]            1140mil       1840mil       1140mil       1840mil   1102.598mil       1840mil  B        360.00 Res Semi       
R19        2012[0805]            1000mil       1680mil       1000mil       1680mil    962.598mil       1680mil  B        360.00 Res Semi       
R21        2012[0805]            1040mil       1760mil       1040mil       1760mil   1002.598mil       1760mil  B        360.00 Res Semi       
R26        3216[1206]             339mil       2000mil        339mil       2000mil    394.118mil       2000mil  B        180.00 Res Semi       
R27        3216[1206]             800mil       1980mil        800mil       1980mil    855.118mil       1980mil  B        180.00 Res Semi       
R29        3216[1206]            1000mil       1980mil       1000mil       1980mil   1055.118mil       1980mil  B        180.00 Res Semi       
R31        3216[1206]             140mil       2000mil        140mil       2000mil    195.118mil       2000mil  B        180.00 Res Semi       
R32        3216[1206]             100mil       2100mil        100mil       2100mil    155.118mil       2100mil  B        180.00 Res Semi       
R36        2012[0805]             380mil        540mil        380mil        540mil    417.402mil        540mil  B        180.00 Res Semi       
U3         DW020_N               1360mil       1360mil       1360mil       1360mil       1585mil   1180.866mil  B         90.00 SN74AHC573DW   
U5         DW020_N                280mil       1140mil        280mil       1140mil        505mil    960.866mil  B         90.00 SN74AHC573DW   
U6         LW18_L                 260mil       1720mil        260mil       1720mil        460mil   1542.834mil  B         90.00 ULN2804LW      
J1         440068             381.338mil    328.347mil    381.338mil   -129.724mil    519.134mil    381.694mil  B        270.00 440068-1       
P1         HDR2X3                2990mil        840mil       2940mil        740mil       2940mil        740mil  B         90.00 MHDR2X3        
U1         SOIC8N_N              2880mil       1980mil       2880mil       1980mil   2785.512mil       1905mil  B        360.00 DS1307Z        

