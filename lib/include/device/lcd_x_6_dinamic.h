#ifdef __lcd_x_6_dinamic_h__
    #warning "Include twice: " __FILE__ , __LINE__
#else
    #define __lcd_x_6_dinamic_h__
/* INSERT HEADER

usage:
> #include <device/lcd_x_6_dinamic.h>
*/

#include <stdint.h>

/* data interface */
typedef enum comand_set_for_lcd_x6_dinamic {
    /*BASIC*/
    NOP = 0,
    LCD_INIT_ON,
    LCD_INIT_OFF,
    LCD_INIT_STATUS,    /*TODO: rename*/
    /**/
    LCD_SET_BUF,
    LCD_SET_MODE,
    LCD_SET_BRIGHTNESS,
    /**/
    LCD_GET_STATUS = LCD_INIT_STATUS,
    }comand_set_for_lcd_x6_dinamic_t;





/*DONE:*/
extern void lcd_init();
extern void lcd_test_string_output(void (*pause)(), uint8_t (*buf_to_display)[] );
extern void lcd_test_value_output(void);
extern void lcd_test_hex_output(void);


#endif /* __lcd_x_6_dinamic_h__ */