/*
 * ----------------------------------------------------------------------------
 * ----------------------------------------------------------------------------
 *
 *
 * $Id$
 */

#include <project.conf>
/*extra macros*/

/* helper, used internally */
#define _force_port_bit_to(name, bit, val)  \
    do { \
        if (val != 0) \
        { \
            PORT ## name |= _BV(PIN ## bit); \
        } \
        else \
        { \
            PORT ## name &= ~(_BV(PIN ## bit)); \
        }; \
    } while (0)
/* interface macros
    /val name - short port name, like A, B, C .... avaliable in target CPU
    /
*/
#define force_port_bit_to(name, num, val) {_force_port_bit_to(name, num, val);}

/* helper, used internally */
#define _force_register_bit_to(fullname, bit, val)  \
    do { \
        if (val != 0) \
        { \
            fullname |= _BV(PIN ## bit); \
        } \
        else \
        { \
            fullname &= ~(_BV(PIN ## bit)); \
        }; \
    } while (0)

/* interface macros
    /val name - short port name, like DDRA, PORTB, other .... avaliable in target CPU
    /
*/
#define force_register_bit_to(fullname, num, val) {_force_register_bit_to(fullname, num, val);}

#define __force_register_bit_to(name, num, val) {_force_register_bit_to(DDR ## name, num, val);}

#define Test(x) force_port_bit_to(TEST_PIN_PORT,TEST_PIN_NUMBER,x)

#define _Test_init(x) \
    { \
        __force_register_bit_to(x, TEST_PIN_NUMBER, 1); \
    }
#define Test_init() _Test_init(TEST_PIN_PORT)
