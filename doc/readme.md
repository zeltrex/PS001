TRACE:
======

# [ ] TODO: setup MakeFIle
# [ ] TODO: HowTo config toolchain (step-by-step)

use for compile/link,hex/debug/lib/program
> sudo apt-get install gcc-avr binutils-avr gdb-avr avr-libc avrdude

minimum set:

use for compilation:
> make clean && make all
use to write to CPU with programmer (EvUSBAsp)

you will need crosc conect P1 to USBASP programmer or use AVRISP 6pin header directly. (as an example http://i1183.photobucket.com/albums/x462/mnemennth/Sunrise%2020A%20and%20Programming%20Adapters/USBasp_PinoutVisual.jpg)
            CPU     >   EvUSBASP
P1-1    >   MISO    >   MISO    >   (9)
P1-2    >   5V      >   NC      >   (2)     * supply PCB externally, do not connect this pin
P1-3    >   SCK     >   SCK     >   (7)
P1-4    >   MOSI    >   MOSI    >   (1)
P1-5    >   \RST    >   |RST    >   (5)
P1-6    >   GND     >   GND     >   (4,6,8,10)


# RS485 setup
see https://www.ftdichip.com/Support/Documents/AppNotes/AN_220_FTDI_Drivers_Installation_Guide_for_Linux.pdf

or:
1. dmesg | grep FTDI
2. sudo rmmod ftdi_sio
3. sudo rmmod usbserial
4. sudo apt install moserial
! inmportant to add to dialaut group
sudo adduser <the user you want to add> dialout
sudo reboot

> See for rs485 modbus eccess from PC
> https://techsparx.com/energy-system/modbus/linux-modbus-usb-rs485.html
> 

> avrdude -p m8 -c usbasp -P usb -U flash:w:SP001.hex
or with single line:
> make clean && make all && avrdude -p m8 -c usbasp -P usb -U flash:w:SP001.hex
use documentation:
>https://www.microchip.com/webdoc/AVRLibcReferenceManual/index.html

DONE: 
- tested _delay_ms() - are not work properly
- used _delay_loop_1 from <util/delay_basic.h> just for test

# [ ] TODO: Organize display as driver

- reuse codes from old version
- review procedures



# [ ] TODO: rewrite doc to md format

#TODO: replace flag to bool type:

```c
    #include <stdbool.h>
    ...
    static _Bool __simple_flag = false;
```



> ...

# documentation
===============

## goals:

- redesign SW for old projects
- increase usability of devices with multy-target configuration
- practice with C code

## schedule:

- [ ] TODO: (1w): tools chain and build system;
- [ ] TODO: (1w): task solving: "CLOCK";
- [ ] TODO: (1w): task solving "PROTOCOL sniffer";
- [ ] TODO: (?w): MODBUS muster/slave/sniffer;

## author:

Company: Zeltrex.com
Person: Vasiliy Golubenko
Contact: Zeltrex.com@gmail.com


