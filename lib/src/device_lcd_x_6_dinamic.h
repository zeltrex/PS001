#ifdef __device_lcd_x_6_dinamic_h__
    #warning "Include twice: " __FILE__ , __LINE__
#else
    #define __device_lcd_x_6_dinamic_h__
/* INSERT HEADER */

/*
internally used only usage:
> #include <device_lcd_x_6_dinamic.h>
*/
/* global includes*/
#include <stdint.h>
#include <avr/pgmspace.h>


/* local includes*/
#include "global.conf"
#include "device_lcd_x_6_dinamic.conf"


/*
TODO: add directive to place data to specific section
TODO:
*/

/* move to types_custom.h*/
typedef uint8_t lcd_output_buffer_value_t;

union lcd_input_buffer_value_u {
uint8_t v;
struct {
#if (LCD_EXTRA_SYMBOLS_USED == 1)
        uint8_t lowbits : 5;               /* 0...3 or 0...4 */
#else
        uint8_t lowbits : 4;               /* 0...3 */
        uint8_t         : 1;               /* 4 */
#endif
        _Bool blinking: 1;                 /* 5 */
        _Bool dot_on: 1;                   /* 6 */
        _Bool blank: 1;                    /* 7 */
};
};
typedef union lcd_input_buffer_value_u lcd_input_buffer_value_t;

/* operation abstraction */
typedef uint8_t interface_t(uint8_t inval, void (*action)(void));
struct menu_line_t{
    const uint8_t name[6];
    const interface_t* const I;
 };
typedef struct menu_line_t *menue_t;

const lcd_output_buffer_value_t lcd_segments_decode_table_arr[] PROGMEM =
{
    0b00111111, /* 0 */
    0b00110000, /* 1 */
    0b01011011, /* 2 */
    0b01111001, /* 3 */
    0b01110100, /* 4 */
    0b01101101, /* 5 */
    0b01101111, /* 6 */
    0b00111000, /* 7 */
    0b01111111, /* 8 */
    0b01111101, /* 9 */
    0b01111110, /* A */
    0b01100111, /* b */
    0b00001111, /* C */
    0b01110011, /* d */
    0b01001111, /* E */
    0b01001110, /* F */
#if (LCD_EXTRA_SYMBOLS_USED == 1)
    /* extra symbols */
    0b00000000, /*(0) space */
    0b01000000, /*(1) - */
    0b00000001, /*(2) _ */
    0b01011100, /*(3) degree */
    0b01011110, /*(4) P */
    0b01000010, /*(5) r */
    0b01100011, /*(6) o */
    0b01000011, /*(7) c */
    0b00000000, /*(8) reserved */
    0b00000000, /*(9) reserved */
    0b00000000, /*(A) reserved */
    0b00000000, /*(B) reserved */
    0b00000000, /*(C) reserved */
    0b00000000, /*(D) reserved */
    0b00000000, /*(E) reserved */
    0b00000000, /*(F) reserved */
#endif
};

#if (LCD_EXTRA_SYMBOLS_USED == 1)
    #define PROTECTION_ARRAY_ACCESS_MASK 0x1F
#else
    #define PROTECTION_ARRAY_ACCESS_MASK 0x0F
#endif
#define LCD_BUZZER_MASK  0b01000000
const uint8_t lcd_digit_decode_table_arr[LCD_DIGITS_COUNT] PROGMEM =
{
    0b11011100, /* 0 */
    0b10111100, /* 1 */
    0b01111100, /* 2 */
    0b11111000, /* 3 */
    0b11110100, /* 4 */
    0b11101100, /* 5 */
//  0b01000000, /* BUZZER */
//  0b10000000, /* NOT USED */
};

#define LCD_DECODE_TABLE_SIZE   sizeof(digit_decode_table_arr)
#define LCD_DOT_MASK    0b10000000


#endif /* __device_lcd_x_6_dinamic_h__ */

