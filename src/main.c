/* global libraries */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay_basic.h>

/* local libraries */
#include <device/lcd_x_6_dinamic.h>

/* local includes */
#include "defines.h"

void basic_board_init()
{
    /*protect from erroneous driving of RS485 outputs */
    force_register_bit_to(PORTB,3,0);
    force_register_bit_to(DDRB,3,1);
    /*Pull_Up buttons INTerrupt input*/
    force_register_bit_to(DDRD,3,0);
    force_register_bit_to(PORTD,3,1);
}

#define SELECTED_TEST 4
int main(void)
{
    cli();
    basic_board_init();

_delay_loop_2(32000u);

#if (0 == SELECTED_TEST)
    /*MAIN LOOP*/
#elif  ( 1 == SELECTED_TEST )
    void pause(void)
        {
            _delay_loop_2(100u);
        };
    lcd_init();
    uint8_t buf[] = {1,2,3,4,5,6};
    while(1)
        {
            lcd_test_string_output(pause, &buf);
        }    /*TEST # 1 */
#elif  (2 == SELECTED_TEST )
    Test_init();
    while(1)
    {
        Test(1);            // PC0 = High = Vcc
        _delay_loop_1(100u);                // wait 500 milliseconds
        Test(0);
        _delay_loop_1(10u);                // wait 500 milliseconds
    }

#elif  (3 == SELECTED_TEST )
    lcd_init();
    while(1)    lcd_test_value_output();
#elif  (4 == SELECTED_TEST )
    lcd_init();
    while(1)    lcd_test_hex_output();
#else
    #error test number error
#endif

}